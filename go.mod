module gitlab.flux.utah.edu/openzms/zms-lib-go

go 1.20

require (
	github.com/google/uuid v1.3.1
	github.com/jxskiss/base62 v1.1.0
	github.com/rs/zerolog v1.31.0
	gitlab.flux.utah.edu/openzms/zms-api/go v0.0.0-20250205210342-6903b0379688
	golang.org/x/exp v0.0.0-20231219180239-dc181d75b848
	google.golang.org/grpc v1.60.1
	google.golang.org/protobuf v1.32.0
)

require (
	github.com/envoyproxy/protoc-gen-validate v1.0.2 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/net v0.16.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20231002182017-d307bd883b97 // indirect
)
