// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package token

import (
	"testing"
)

func TestGenerate(t *testing.T) {
	_, err := Generate(PersonalAccessToken)
	if err != nil {
		t.Fatalf(`Generate(PersonalAccessToken) failed: %v`, err)
	}
}

func TestValidateDynamic(t *testing.T) {
	token, err := Generate(PersonalAccessToken)
	if err != nil {
		t.Fatalf(`Generate(PersonalAccessToken) failed: %v`, err)
	}
	err = Validate(token)
	if err != nil {
		t.Fatalf(`Validate(%s) failed: %v`, token, err)
	}
}

func TestValidateStaticUserSuccess(t *testing.T) {
	token := "rpu_Ik89eH2peCdNlImcVx9vKawgoxzoAWQvcYf6YjJ1KwQHXyoTqe"
	err := Validate(token)
	if err != nil {
		t.Fatalf(`Validate(%s) failed: %v`, token, err)
	}
}

func TestValidateStaticPatSuccess(t *testing.T) {
	token := "rpp_Ps7kpe0B0S8CxLtpT0oXYZbwM41trWwKUf3XAJocFhJQPrqfEe"
	err := Validate(token)
	if err != nil {
		t.Fatalf(`Validate(%s) failed: %v`, token, err)
	}
}

func TestValidateStaticServiceSuccess(t *testing.T) {
	token := "rps_CgufRkf7Rgv5338SoHhxG2dCZ65B5MqeafeYUL4DoZXabJhclDB"
	err := Validate(token)
	if err != nil {
		t.Fatalf(`Validate(%s) failed: %v`, token, err)
	}
}

func TestValidateBrokenPrefixFailure(t *testing.T) {
	token := "rpz_Ik89eH2peCdNlImcVx9vKawgoxzoAWQvcYf6YjJ1KwQHXyoTqe"
	err := Validate(token)
	if err == nil {
		t.Fatalf(`Validate(%s) did not fail!`, token)
	} else {
		t.Logf(`Validate(%s) failed as expected: %v`, token, err)
	}
}

func TestValidateBrokenTooLongFailure(t *testing.T) {
	token := "rpu_Ik89eH2peCdNlImcVx9vKawgoxzoAWQvcYf6YjJ1KwQHXyoTqe1"
	err := Validate(token)
	if err == nil {
		t.Fatalf(`Validate(%s) did not fail!`, token)
	} else {
		t.Logf(`Validate(%s) failed as expected: %v`, token, err)
	}
}

func TestValidateBrokenCrcFailure(t *testing.T) {
	token := "rpu_Ik89eH2peCdNlImcVx9vKawgoxzoAWRvcYf6YjJ1KwQHXyoTqe"
	err := Validate(token)
	if err == nil {
		t.Fatalf(`Validate(%s) did not fail!`, token)
	} else {
		t.Logf(`Validate(%s) failed as expected: %v`, token, err)
	}
}
