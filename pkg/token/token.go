// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package token

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/binary"
	"errors"
	"fmt"
	"hash/crc32"
	"strings"

	"github.com/jxskiss/base62"
)

type TokenType int

const (
	UserToken TokenType = iota
	PersonalAccessToken
	ServiceToken
)

var validPrefixes = map[string]string{
	"rpu": "user-to-server token",
	"rpp": "personal access token",
	"rps": "service token",
}

var prefixMap = map[TokenType]string{
	UserToken:           "rpu",
	PersonalAccessToken: "rpp",
	ServiceToken:        "rps",
}

// Approach shamelessly ripped from Github:
// prefix + "_" + base62(sha256(32) + crc32(4))
func Generate(tt TokenType) (token string, err error) {
	if tt < UserToken || tt > ServiceToken {
		return "", fmt.Errorf("invalid token type (%d)", tt)
	}
	prefix := prefixMap[tt]
	b := make([]byte, 32)
	_, err = rand.Read(b)
	if err != nil {
		return "", err
	}
	h := sha256.New()
	h.Write(b)
	b = h.Sum(nil)
	s := crc32.ChecksumIEEE(b)
	sbuf := make([]byte, binary.MaxVarintLen32)
	binary.PutUvarint(sbuf, uint64(s))
	t := prefix + "_" + base62.EncodeToString(append(b, sbuf...))
	return t, nil
}

func Validate(token string) error {
	prefix, enc, found := strings.Cut(token, "_")
	if !found {
		return errors.New("malformed token: missing prefix separator")
	}
	if _, ok := validPrefixes[prefix]; !ok {
		return errors.New("malformed token: invalid prefix")
	}
	dec, err := base62.DecodeString(enc)
	if err != nil {
		return fmt.Errorf("malformed token: invalid base62: %w", err)
	} else if len(dec) != (256/8 + binary.MaxVarintLen32) {
		return errors.New("malformed token: invalid size")
	}
	dsum, _ := binary.Uvarint(dec[32:])
	osum := crc32.ChecksumIEEE(dec[0:32])
	if dsum != uint64(osum) {
		return errors.New("malformed token: checksum does not match")
	}

	return nil
}
