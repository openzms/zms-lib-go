// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package policy

import (
	"github.com/google/uuid"
)

const (
	RoleAdmin    int = 1000
	RoleOwner    int = 900
	RoleManager  int = 800
	RoleOperator int = 500
	RoleMonitor  int = 400
	RoleMember   int = 200
	RoleViewer   int = 100
	RoleAccount  int = 75
	RoleService  int = 50
)

type Role struct {
	Name        string
	Value       int
	Description string
}

var DefaultRoles = [...]Role{
	Role{Name: "admin", Value: RoleAdmin, Description: "Grants administrator permission to invoke any API."},
	Role{Name: "owner", Value: RoleOwner, Description: "Grants access to all non-administrator APIs within an Element."},
	Role{Name: "manager", Value: RoleManager, Description: "Grants access to all non-administrator, non-owner APIs within an Element."},
	Role{Name: "operator", Value: RoleOperator, Description: "Granted to Element endpoint service users; allows invocation of RDZ operational APIs."},
	Role{Name: "monitor", Value: RoleMonitor, Description: "Granted to Element endpoint service users; allows invocation of RDZ operational APIs."},
	Role{Name: "member", Value: RoleMember, Description: "Grants access to non-administrative Element operations."},
	Role{Name: "viewer", Value: RoleViewer, Description: "Grants access to monitor operations and status, but cannot create or modify RDZ objects."},
	Role{Name: "account", Value: RoleAccount, Description: "Grants access to basic account operations; asserts user approval."},
	Role{Name: "service", Value: RoleService, Description: "Granted to Element endpoint service users; allows invocation of RDZ operational APIs."},
}

type NumericComparator int

const (
	AnyNumber NumericComparator = iota
	Equal
	Less
	Greater
	LessOrEqual
	GreaterOrEqual
)

type BooleanComparator int

const (
	AnyBoolean BooleanComparator = iota
	Match
)

type Policy struct {
	Name              string
	Role              int
	RoleComparator    NumericComparator
	UserComparator    BooleanComparator
	ElementComparator BooleanComparator
}

func (p *Policy) Check(callingRole int, callingUserId *uuid.UUID, callingElementId *uuid.UUID, targetUserId *uuid.UUID, targetElementId *uuid.UUID) bool {
	switch p.RoleComparator {
	case Equal:
		if callingRole != p.Role {
			return false
		}
	case Less:
		if callingRole >= p.Role {
			return false
		}
	case Greater:
		if callingRole <= p.Role {
			return false
		}
	case LessOrEqual:
		if callingRole > p.Role {
			return false
		}
	case GreaterOrEqual:
		if callingRole < p.Role {
			return false
		}
	}

	switch p.UserComparator {
	case Match:
		if callingUserId == nil || targetUserId == nil {
			return false
		}
		if *callingUserId != *targetUserId {
			return false
		}
	}

	switch p.ElementComparator {
	case Match:
		if callingElementId == nil || targetElementId == nil {
			return false
		}
		if *callingElementId != *targetElementId {
			return false
		}
	}

	return true
}

type PolicyContext struct {
	CallingRole      int
	CallingUserId    *uuid.UUID
	CallingElementId *uuid.UUID
	TargetUserId     *uuid.UUID
	TargetElementId  *uuid.UUID
}

func MatchFirst(ctx PolicyContext, policies []Policy) (bool, string) {
	for _, p := range policies {
		if match := p.Check(ctx.CallingRole, ctx.CallingUserId, ctx.CallingElementId, ctx.TargetUserId, ctx.TargetElementId); match == true {
			return true, p.Name
		}
	}
	return false, ""
}

func MatchFirstPolicy(ctx PolicyContext, policies []Policy) (bool, *Policy) {
	for _, p := range policies {
		if match := p.Check(ctx.CallingRole, ctx.CallingUserId, ctx.CallingElementId, ctx.TargetUserId, ctx.TargetElementId); match == true {
			return true, &p
		}
	}
	return false, nil
}

const MatchViewer string = "match-viewer"
const MatchUser string = "match-user"
const MatchUserRole string = "match-user-role"
const MatchUserElementRole string = "match-user-element-role"
const MatchElementRole string = "match-element-role"
const MatchAdmin string = "match-admin"

// The most common policy: calling user must match target user, or user must
// be admin.
var MatchUserOrAdminPolicy []Policy = []Policy{
	Policy{Name: MatchUser, UserComparator: Match},
	Policy{Name: MatchAdmin, Role: RoleAdmin, RoleComparator: GreaterOrEqual},
}

// Another common policy: calling user be admin.
var AdminOnlyPolicy []Policy = []Policy{
	Policy{Name: MatchAdmin, Role: RoleAdmin, RoleComparator: GreaterOrEqual},
}

// Another common policy: calling user have at least the most basic role.
var ViewerPolicy []Policy = []Policy{
	Policy{Name: MatchViewer, Role: RoleViewer, RoleComparator: GreaterOrEqual},
}

// Another common policy: calling user must match target user with specific
// role, or user must be admin.
func MakeMatchAdminOrUserRolePolicy(role int, comp NumericComparator) []Policy {
	return []Policy{
		Policy{Name: MatchAdmin, Role: RoleAdmin, RoleComparator: GreaterOrEqual},
		Policy{Name: MatchUserRole, Role: role, RoleComparator: comp, UserComparator: Match},
	}
}

// Another common policy: calling user must have a role in the
// target element.
func MakeMatchAdminOrElementRolePolicy(role int, comp NumericComparator) []Policy {
	return []Policy{
		Policy{Name: MatchAdmin, Role: RoleAdmin, RoleComparator: GreaterOrEqual},
		Policy{Name: MatchElementRole, Role: role, RoleComparator: comp, ElementComparator: Match},
	}
}

// Another common policy: calling user must have a role in the
// target element -- or any viewer is allowed.
func MakeMatchAdminOrElementRoleOrViewerPolicy(role int, comp NumericComparator) []Policy {
	return []Policy{
		Policy{Name: MatchAdmin, Role: RoleAdmin, RoleComparator: GreaterOrEqual},
		Policy{Name: MatchElementRole, Role: role, RoleComparator: comp, ElementComparator: Match},
		Policy{Name: MatchViewer, Role: RoleViewer, RoleComparator: GreaterOrEqual},
	}
}

// Another common policy: calling user must have a role in the
// target element, or user must own target resource.
func MakeMatchAdminOrElementRoleOrUserPolicy(role int, comp NumericComparator) []Policy {
	return []Policy{
		Policy{Name: MatchAdmin, Role: RoleAdmin, RoleComparator: GreaterOrEqual},
		Policy{Name: MatchElementRole, Role: role, RoleComparator: comp, ElementComparator: Match},
		Policy{Name: MatchUser, UserComparator: Match},
	}
}

// Another common policy: calling user must have a role in the
// target element -- or any viewer is allowed.
func MakeMatchAdminOrElementRoleOrUserOrViewerPolicy(role int, comp NumericComparator) []Policy {
	return []Policy{
		Policy{Name: MatchAdmin, Role: RoleAdmin, RoleComparator: GreaterOrEqual},
		Policy{Name: MatchElementRole, Role: role, RoleComparator: comp, ElementComparator: Match},
		Policy{Name: MatchUser, UserComparator: Match},
		Policy{Name: MatchViewer, Role: RoleViewer, RoleComparator: GreaterOrEqual},
	}
}

// Another common policy: calling user must have a specific role in the
// target element.
func MakeMatchAdminOrElementRoleOrUserElementRolePolicy(elementRole int, elementComp NumericComparator, userRole int, userComp NumericComparator) []Policy {
	return []Policy{
		Policy{Name: MatchAdmin, Role: RoleAdmin, RoleComparator: GreaterOrEqual},
		Policy{Name: MatchElementRole, Role: elementRole, RoleComparator: elementComp, ElementComparator: Match},
		Policy{Name: MatchUserElementRole, Role: userRole, RoleComparator: userComp, UserComparator: Match, ElementComparator: Match},
	}
}
