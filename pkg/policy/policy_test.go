// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package policy

import (
	"github.com/google/uuid"
	"testing"
)

var callingUserId, _ = uuid.Parse("b9b86923-1be0-435d-9ee5-1a5956d6329d")
var notCallingUserId, _ = uuid.Parse("172a9543-cde7-4c1b-83c2-2a0b0bb9f1c9")
var callingElementId, _ = uuid.Parse("f952d81b-d1a7-4726-8a7c-09dfec8bc98f")
var notCallingElementId, _ = uuid.Parse("b44ec360-6ea1-4640-b6bd-f91ceccf3120")

func TestAdminOnlyPolicySuccess(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleAdmin,
		CallingUserId:    &callingUserId,
		CallingElementId: nil,
		TargetUserId:     &callingUserId,
		TargetElementId:  nil,
	}
	match, name := MatchFirst(ctx, AdminOnlyPolicy)
	if !match {
		t.Fatalf(`TestAdminOnlyPolicySuccess failed to match`)
	} else if name != MatchAdmin {
		t.Fatalf(`TestAdminOnlyPolicySuccess failed to match correct policy (%s)`, name)
	}
}

func TestAdminOnlyPolicyFailure(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleViewer,
		CallingUserId:    &callingUserId,
		CallingElementId: nil,
		TargetUserId:     &callingUserId,
		TargetElementId:  nil,
	}
	match, name := MatchFirst(ctx, AdminOnlyPolicy)
	if match {
		t.Fatalf(`TestAdminOnlyPolicyFailure falsely matched policy %s`, name)
	}
}

func TestMatchUserOrAdminPolicySuccess(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleViewer,
		CallingUserId:    &callingUserId,
		CallingElementId: nil,
		TargetUserId:     &callingUserId,
		TargetElementId:  nil,
	}
	match, name := MatchFirst(ctx, MatchUserOrAdminPolicy)
	if !match {
		t.Fatalf(`TestMatchUserOrAdminPolicySuccess failed to match`)
	} else if name != MatchUser {
		t.Fatalf(`TestMatchUserOrAdminPolicySuccess failed to match correct policy (%s)`, name)
	}
}

func TestMatchUserOrAdminPolicyFailure(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleViewer,
		CallingUserId:    &callingUserId,
		CallingElementId: nil,
		TargetUserId:     &notCallingUserId,
		TargetElementId:  nil,
	}
	match, name := MatchFirst(ctx, MatchUserOrAdminPolicy)
	if match {
		t.Fatalf(`TestMatchUserOrAdminPolicyFailure falsely matched policy %s`, name)
	}
}

func TestMakeMatchAdminOrUserRolePolicyFailure(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleViewer,
		CallingUserId:    &callingUserId,
		TargetUserId:     &notCallingUserId,
	}
	policies := MakeMatchAdminOrUserRolePolicy(RoleAccount, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if match {
		t.Fatalf(`TestMakeMatchAdminOrUserRolePolicyFailure falsely matched policy %s`, name)
	}
}

func TestMakeMatchAdminOrUserRolePolicyFailure2(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleMember,
		CallingUserId:    &callingUserId,
		TargetUserId:     &notCallingUserId,
	}
	policies := MakeMatchAdminOrUserRolePolicy(RoleViewer, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if match {
		t.Fatalf(`TestMakeMatchAdminOrUserRolePolicyFailure2 falsely matched policy %s`, name)
	}
}

func TestMakeMatchAdminOrUserRolePolicyFailure3(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleMember,
		CallingUserId:    &callingUserId,
		TargetUserId:     &notCallingUserId,
	}
	policies := MakeMatchAdminOrUserRolePolicy(RoleMember, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if match {
		t.Fatalf(`TestMakeMatchAdminOrUserRolePolicyFailure3 falsely matched policy (%s)`, name)
	}
}

func TestMakeMatchAdminOrUserRolePolicySuccess(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleMember,
		CallingUserId:    &callingUserId,
		TargetUserId:     &callingUserId,
	}
	policies := MakeMatchAdminOrUserRolePolicy(RoleMember, GreaterOrEqual)
	match, _ := MatchFirst(ctx, policies)
	if !match {
		t.Fatalf(`TestMakeMatchAdminOrUserRolePolicySuccess failed to match policy`)
	}
}

func TestMakeMatchAdminOrUserRolePolicySuccess2(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleViewer,
		CallingUserId:    &callingUserId,
		TargetUserId:     &callingUserId,
	}
	policies := MakeMatchAdminOrUserRolePolicy(RoleAccount, GreaterOrEqual)
	match, _ := MatchFirst(ctx, policies)
	if !match {
		t.Fatalf(`TestMakeMatchAdminOrUserRolePolicySuccess2 failed to match policy`)
	}
}


func TestMakeMatchAdminOrElementRoleOrViewerPolicySuccess(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleViewer,
		CallingUserId:    &callingUserId,
		CallingElementId: nil,
		TargetUserId:     &callingUserId,
		TargetElementId:  nil,
	}
	policies := MakeMatchAdminOrElementRoleOrViewerPolicy(RoleViewer, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if !match {
		t.Fatalf(`TestMakeMatchAdminOrElementRoleOrViewerPolicySuccess failed to matched policy (%s)`, name)
	}
}

func TestMakeMatchAdminOrElementRoleOrViewerPolicySuccess2(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleViewer,
		CallingUserId:    &callingUserId,
		CallingElementId: nil,
		TargetUserId:     &callingUserId,
		TargetElementId:  nil,
	}
	policies := MakeMatchAdminOrElementRoleOrViewerPolicy(0, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if !match {
		t.Fatalf(`TestMakeMatchAdminOrElementRoleOrViewerPolicySuccess2 failed to matched policy (%s)`, name)
	}
}

func TestMakeMatchAdminOrElementRoleOrUserElementRolePolicyFailure(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleViewer,
		CallingUserId:    &callingUserId,
		CallingElementId: &callingElementId,
		TargetUserId:     &notCallingUserId,
		TargetElementId:  &callingElementId,
	}
	policies := MakeMatchAdminOrElementRoleOrUserElementRolePolicy(RoleManager, GreaterOrEqual, RoleMember, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if match {
		t.Fatalf(`TestMakeMatchAdminOrElementRoleOrUserElementRolePolicyFailure falsely matched policy %s`, name)
	}
}

func TestMakeMatchAdminOrElementRoleOrUserElementRolePolicyFailure2(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleMember,
		CallingUserId:    &callingUserId,
		CallingElementId: &callingElementId,
		TargetUserId:     &notCallingUserId,
		TargetElementId:  &notCallingElementId,
	}
	policies := MakeMatchAdminOrElementRoleOrUserElementRolePolicy(RoleManager, GreaterOrEqual, RoleMember, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if match {
		t.Fatalf(`TestMakeMatchAdminOrElementRoleOrUserElementRolePolicyFailure2 falsely matched policy %s`, name)
	}
}

func TestMakeMatchAdminOrElementRoleOrUserElementRolePolicyFailure3(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleMember,
		CallingUserId:    &callingUserId,
		CallingElementId: &callingElementId,
		TargetUserId:     &notCallingUserId,
		TargetElementId:  &callingElementId,
	}
	policies := MakeMatchAdminOrElementRoleOrUserElementRolePolicy(RoleManager, GreaterOrEqual, RoleMember, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if match {
		t.Fatalf(`TestMakeMatchAdminOrElementRoleOrUserElementRolePolicyFailure3 falsely matched policy (%s)`, name)
	}
}

func TestMakeMatchAdminOrElementRoleOrUserElementRolePolicySuccess(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleMember,
		CallingUserId:    &callingUserId,
		CallingElementId: &callingElementId,
		TargetUserId:     &callingUserId,
		TargetElementId:  &callingElementId,
	}
	policies := MakeMatchAdminOrElementRoleOrUserElementRolePolicy(RoleManager, GreaterOrEqual, RoleMember, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if !match {
		t.Fatalf(`TestMakeMatchAdminOrElementRoleOrUserElementRolePolicySuccess failed to match policy (%s)`, name)
	}
}

func TestMakeMatchAdminOrElementRoleOrUserElementRolePolicySuccess2(t *testing.T) {
	ctx := PolicyContext{
		CallingRole:      RoleManager,
		CallingUserId:    &callingUserId,
		CallingElementId: &callingElementId,
		TargetUserId:     &notCallingUserId,
		TargetElementId:  &callingElementId,
	}
	policies := MakeMatchAdminOrElementRoleOrUserElementRolePolicy(RoleManager, GreaterOrEqual, RoleMember, GreaterOrEqual)
	match, name := MatchFirst(ctx, policies)
	if !match {
		t.Fatalf(`TestMakeMatchAdminOrElementRoleOrUserElementRolePolicySuccess2 failed to match policy (%s)`, name)
	}
}
