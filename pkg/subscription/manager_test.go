// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package subscription

import (
	"testing"

	"github.com/google/uuid"
)

var sm *SubscriptionManager[*Event] = NewSubscriptionManager[*Event]()
var ch chan *Event = make(chan *Event)
var oid string = uuid.New().String()
var e Event = Event{
	Header: EventHeader{
		Type:       1,
		Code:       2,
		SourceType: 1,
		SourceId:   uuid.New().String(),
		Id:         uuid.New().String(),
		ObjectId:   &oid,
	},
	Object: "foo event",
}

func TestGetSubscriptionsZero(t *testing.T) {
	c := sm.GetSubscriptionCount()
	if c != 0 {
		t.Fatalf(`GetSubscriptionCount nonzero (%d)`, c)
	}
}

func TestSubscribe(t *testing.T) {
	if _, err := sm.Subscribe("foo", nil, ch, "none", Unspecified, nil); err != nil {
		t.Fatalf(`Subscribe failed: %+v`, err)
	}
	c := sm.GetSubscriptionCount()
	if c != 1 {
		t.Fatalf(`GetSubscriptionCount expected 1 (%d)`, c)
	}
	ss := sm.GetSubscriptions()
	if ss == nil || len(ss) != 1 {
		t.Fatalf(`GetSubscriptions should have returned single element slice (%+v)`, ss)
	}
	if ss[0].Id != "foo" || ss[0].Filters != nil || ss[0].Output != ch || ss[0].Endpoint != "none" || ss[0].EndpointType != Unspecified || ss[0].Token != nil {
		t.Fatalf(`single subscription does not match input (%+v)`, ss[0])
	}
}

func TestSubscribeDuplicate(t *testing.T) {
	if _, err := sm.Subscribe("foo", nil, ch, "none", Unspecified, nil); err == nil {
		t.Fatalf(`SubscribeDuplicate should have failed`)
	}
}

func TestUpdateChannelNil(t *testing.T) {
	if err := sm.UpdateChannel("foo", nil, nil); err != nil {
		t.Fatalf(`UpdateChannel failed: %+v`, err)
	}
	c := sm.GetSubscriptionCount()
	if c != 1 {
		t.Fatalf(`GetSubscriptionCount expected 1 (%d)`, c)
	}
	ss := sm.GetSubscriptions()
	if ss == nil || len(ss) != 1 {
		t.Fatalf(`GetSubscriptions should have returned single element slice (%+v)`, ss)
	}
	if ss[0].Id != "foo" || ss[0].Filters != nil || ss[0].Output != nil || ss[0].Endpoint != "none" || ss[0].EndpointType != Unspecified || ss[0].Token != nil {
		t.Fatalf(`single subscription does not match input (%+v)`, ss[0])
	}
}

func TestUpdateChannel(t *testing.T) {
	if err := sm.UpdateChannel("foo", ch, nil); err != nil {
		t.Fatalf(`UpdateChannel failed: %+v`, err)
	}
	c := sm.GetSubscriptionCount()
	if c != 1 {
		t.Fatalf(`GetSubscriptionCount expected 1 (%d)`, c)
	}
	ss := sm.GetSubscriptions()
	if ss == nil || len(ss) != 1 {
		t.Fatalf(`GetSubscriptions should have returned single element slice (%+v)`, ss)
	}
	if ss[0].Id != "foo" || ss[0].Filters != nil || ss[0].Output != ch || ss[0].Endpoint != "none" || ss[0].EndpointType != Unspecified || ss[0].Token != nil {
		t.Fatalf(`single subscription does not match input (%+v)`, ss[0])
	}
}

func TestGetSubscriptionCount(t *testing.T) {
	c := sm.GetSubscriptionCount()
	if c != 1 {
		t.Fatalf(`GetSubscriptionCount not 1 (%d)`, c)
	}
}

func TestSafeOutputClosedChannel(t *testing.T) {
	sch := make(chan *Event)
	close(sch)
	if err := sm.SafeOutput(nil, sch); err == nil {
		t.Fatalf(`SafeOutput did not return error on nil chan`)
	} else {
		t.Logf(`SafeOutput properly returned error: %+v`, err)
	}
}

func TestNotify(t *testing.T) {
	var re *Event
	go func() {
		re = <-ch
	}()
	if err := sm.Notify(&e); err != nil {
		t.Fatalf(`Notify failed with error: %+v`, err)
	}
	if re != &e {
		t.Fatalf(`Notify: event mismatch: expected %+v, received %+v`, &e, re)
	}
}

func TestUnsubscribe(t *testing.T) {
	if err := sm.Unsubscribe("foo"); err != nil {
		t.Fatalf(`Unsubscribe failed: %+v`, err)
	}
	c := sm.GetSubscriptionCount()
	if c != 0 {
		t.Fatalf(`GetSubscriptionCount expected zero (%d)`, c)
	}
	ss := sm.GetSubscriptions()
	if len(ss) != 0 {
		t.Fatalf(`GetSubscriptions should have returned empty list (%+v)`, ss)
	}
}

func TestUnsubscribeList(t *testing.T) {
	if _, err := sm.Subscribe("foo", nil, ch, "none", Unspecified, nil); err != nil {
		t.Fatalf(`Subscribe failed: %+v`, err)
	}
	if _, err := sm.Subscribe("bar", nil, nil, "none", Unspecified, nil); err != nil {
		t.Fatalf(`Subscribe failed: %+v`, err)
	}
	if err := sm.UnsubscribeList([]string{"foo", "bar"}); err != nil {
		t.Fatalf(`UnsubscribeList failed: %+v`, err)
	}
	c := sm.GetSubscriptionCount()
	if c != 0 {
		t.Fatalf(`GetSubscriptionCount expected zero (%d)`, c)
	}
	ss := sm.GetSubscriptions()
	if len(ss) != 0 {
		t.Fatalf(`GetSubscriptions should have returned empty list (%+v)`, ss)
	}
}

func TestUnsubscribeListPartial(t *testing.T) {
	if _, err := sm.Subscribe("foo", nil, ch, "none", Unspecified, nil); err != nil {
		t.Fatalf(`Subscribe failed: %+v`, err)
	}
	if _, err := sm.Subscribe("bar", nil, nil, "none", Unspecified, nil); err != nil {
		t.Fatalf(`Subscribe failed: %+v`, err)
	}
	if _, err := sm.Subscribe("baz", nil, nil, "none", Unspecified, nil); err != nil {
		t.Fatalf(`Subscribe failed: %+v`, err)
	}
	if err := sm.UnsubscribeList([]string{"foo", "bar"}); err != nil {
		t.Fatalf(`UnsubscribeList failed: %+v`, err)
	}
	c := sm.GetSubscriptionCount()
	if c != 1 {
		t.Fatalf(`GetSubscriptionCount expected 1 (%d)`, c)
	}
	ss := sm.GetSubscriptions()
	if len(ss) != 1 {
		t.Fatalf(`GetSubscriptions should have returned single-element list (%+v)`, ss)
	}
	// Unsubscribe the remaining one.
	if err := sm.UnsubscribeList([]string{"baz"}); err != nil {
		t.Fatalf(`UnsubscribeList failed: %+v`, err)
	}
	c = sm.GetSubscriptionCount()
	if c != 0 {
		t.Fatalf(`GetSubscriptionCount expected 0 (%d)`, c)
	}
	ss = sm.GetSubscriptions()
	if len(ss) != 0 {
		t.Fatalf(`GetSubscriptions should have returned empty list (%+v)`, ss)
	}
}

func TestUnsubscribeListFail(t *testing.T) {
	if _, err := sm.Subscribe("foo", nil, ch, "none", Unspecified, nil); err != nil {
		t.Fatalf(`Subscribe failed: %+v`, err)
	}
	if _, err := sm.Subscribe("bar", nil, nil, "none", Unspecified, nil); err != nil {
		t.Fatalf(`Subscribe failed: %+v`, err)
	}
	if err := sm.UnsubscribeList([]string{"baz"}); err == nil {
		t.Fatalf(`UnsubscribeList should have failed`)
	}
	// Unsubscribe the remaining one.
	if err := sm.UnsubscribeList([]string{"foo", "bar"}); err != nil {
		t.Fatalf(`UnsubscribeList failed: %+v`, err)
	}
	c := sm.GetSubscriptionCount()
	if c != 0 {
		t.Fatalf(`GetSubscriptionCount expected 0 (%d)`, c)
	}
	ss := sm.GetSubscriptions()
	if len(ss) != 0 {
		t.Fatalf(`GetSubscriptions should have returned empty list (%+v)`, ss)
	}
}
