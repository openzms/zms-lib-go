// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

// This package provides a generic `SubscriptionManager` for northbound and
// southbound OpenZMS services, based on the gRPC API definitions at
// gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1 .  OpenZMS events
// utilize a common header format that must be part of each event, but the
// users of this package and the API must define the semantics of the event.
// This library accepts any kind of event that provides the basic `EventIf`
// interface, and provides a default `Event` implementation with a single
// `Object interface{}`, giving users a trivial method to define their own
// events.  It does require use of its `EventHeader` type, but this
// requirement could be relaxed via interface if necessary.
package subscription

import (
	"fmt"
	"sync"

	"github.com/rs/zerolog/log"
	"golang.org/x/exp/maps"
)

// Defines the type of subscriber.
type EndpointType string

const (
	Unspecified EndpointType = "unspecified"
	Rest        EndpointType = "rest"
	Rpc         EndpointType = "rpc"
)

// Describes a subscription.
type Subscription[E EventIf] struct {
	Id           string        `json:"id"`
	Filters      []EventFilter `json:"filters"`
	Output       chan E        `json:"-"`
	Endpoint     string        `json:"endpoint"`
	EndpointType EndpointType  `json:"endpoint_type"`
	Token        *string       `json:"-"`
}

// The `SubscriptionManager` type, generic but constrained to the `EventIf`
// interface, provides a typical subscription model.
type SubscriptionManager[E EventIf] struct {
	mutex  sync.RWMutex
	dbId   map[string]*Subscription[E]
	dbChan map[*chan E]*Subscription[E]
}

// Creates a new `SubscriptionManager` for `EventIf`-providing events.
func NewSubscriptionManager[E EventIf]() *SubscriptionManager[E] {
	sm := SubscriptionManager[E]{}
	//sm.list = make([]Subscription[E], 0)
	sm.dbId = make(map[string]*Subscription[E])
	sm.dbChan = make(map[*chan E]*Subscription[E])
	return &sm
}

func (sm *SubscriptionManager[E]) Shutdown() {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	for id, sub := range sm.dbId {
		log.Debug().Msg(fmt.Sprintf("Shutdown: closing subscription %s", id))
		delete(sm.dbChan, &sub.Output)
		delete(sm.dbId, id)
		close(sub.Output)
		log.Debug().Msg(fmt.Sprintf("Shutdown: closed subscription %s", id))
	}
	return
}

// Create a subscription, enter into lookup table, with a queue that is
// being listened on from the stream origin point in nb or sb server.  Post
// matching events to the queue(s) in goroutine calls, which run the queue
// in the stream origin.  Ah, it's not a queue, it's just a channel in
// golang.
func (sm *SubscriptionManager[E]) Subscribe(id string, filters []EventFilter, output chan E, endpoint string, endpointType EndpointType, token *string) (*Subscription[E], error) {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	if _, exists := sm.dbId[id]; exists {
		return nil, fmt.Errorf("subscription " + id + " already exists")
	}
	//sm.list = append(sm.list, Subscription[E]{Filters: filters, Output: output})
	sub := Subscription[E]{
		Id:           id,
		Filters:      filters,
		Output:       output,
		Endpoint:     endpoint,
		EndpointType: endpointType,
		Token:        token,
	}
	sm.dbId[id] = &sub
	if output != nil {
		sm.dbChan[&output] = &sub
	}
	log.Debug().Msg(fmt.Sprintf("Subscribe(%s, %+v)", id, filters))
	return &sub, nil
}

func (sm *SubscriptionManager[E]) UpdateChannel(id string, output chan E, token *string) error {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	if _, exists := sm.dbId[id]; !exists {
		return fmt.Errorf("subscription " + id + " already exists")
	} else if (sm.dbId[id].Token != nil && token == nil) || (sm.dbId[id].Token == nil && token != nil) {
		return fmt.Errorf("unauthorized to update (" + id + ")")
	} else if token != nil && *sm.dbId[id].Token != *token {
		return fmt.Errorf("unauthorized to update (" + id + ")")
	}
	if sm.dbId[id].Output != nil && sm.dbId[id].Output != output {
		delete(sm.dbChan, &sm.dbId[id].Output)
	}
	sm.dbId[id].Output = output
	if output != nil {
		sm.dbChan[&output] = sm.dbId[id]
	}

	return nil
}

func (sm *SubscriptionManager[E]) GetSubscriptionCount() int {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()

	count := len(sm.dbId)
	return count
}

func (sm *SubscriptionManager[E]) GetSubscription(id string) (s *Subscription[E]) {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()

	s, _ = sm.dbId[id]
	return s
}

func (sm *SubscriptionManager[E]) GetSubscriptions() (ss []*Subscription[E]) {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()

	ss = maps.Values(sm.dbId)
	return ss
}

func (sm *SubscriptionManager[E]) unsubscribeList(idList []string) error {
	if idList == nil {
		return nil
	}

	for _, id := range idList {
		if _, exists := sm.dbId[id]; !exists {
			return fmt.Errorf("subscription " + id + " does not exist")
		}
	}
	for _, id := range idList {
		delete(sm.dbChan, &sm.dbId[id].Output)
		delete(sm.dbId, id)
	}
	log.Debug().Msg(fmt.Sprintf("unsubscribeList(%+v)", idList))

	return nil
}

func (sm *SubscriptionManager[E]) UnsubscribeList(idList []string) error {
	if idList == nil {
		return nil
	}
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	return sm.unsubscribeList(idList)
}

func (sm *SubscriptionManager[E]) Unsubscribe(id string) error {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	if _, exists := sm.dbId[id]; !exists {
		return fmt.Errorf("subscription " + id + " does not exist")
	}
	delete(sm.dbChan, &sm.dbId[id].Output)
	delete(sm.dbId, id)
	log.Debug().Msg(fmt.Sprintf("Unsubscribe(%s)", id))
	return nil
}

func (sm *SubscriptionManager[E]) SafeOutput(event E, output chan E) (err error) {
	defer func() {
		if recover() != nil {
			err = fmt.Errorf("failed write to channel")
			log.Error().Err(err).Msg(fmt.Sprintf("SafeOutput error: %s", err.Error()))
		}
	}()

	log.Debug().Msg(fmt.Sprintf("output %+v", output))

	output <- event

	return err
}

func (sm *SubscriptionManager[E]) NotifyMany(events []E) {
	for _, event := range events {
		sm.Notify(event)
	}
}

func (sm *SubscriptionManager[E]) Notify(event E) error {
	header := event.GetHeader()
	if header == nil {
		return nil
	}

	failedIdList := make([]string, 0)
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()

	for _, s := range sm.dbId {
		if s.Filters != nil {
			match := false
			for _, f := range s.Filters {
				if match = f.Match(header); match {
					break
				}
			}
			if !match {
				continue
			}
		}
		if s.Output == nil {
			log.Debug().Msg(fmt.Sprintf("Notify(sub=%s): nil output (unused subscription)", s.Id))
			continue
		}
		if err := sm.SafeOutput(event, s.Output); err != nil {
			log.Debug().Msg(fmt.Sprintf("Notify(sub=%s, id=%s) failed: %s; removing", s.Id, header.Id, err.Error()))
			failedIdList = append(failedIdList, s.Id)
		} else {
			log.Debug().Msgf("Notify(sub=%s, id=%s): type=%s code=%v oid=%+v uid=%+v eid=%+v", s.Id, header.Id, EventTypeString(header.Type), header.Code, header.ObjectId, header.UserId, header.ElementId)
		}
	}

	if len(failedIdList) > 0 {
		sm.unsubscribeList(failedIdList)
	}

	return nil
}
