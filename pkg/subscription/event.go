// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package subscription

import (
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"

	event_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"
)

// A default implementation of `zms.event.v1.EventHeader`.
type EventHeader struct {
	Type       int32      `json:"type"`
	Code       int32      `json:"code"`
	SourceType int32      `json:"source_type"`
	SourceId   string     `json:"source_id"`
	Id         string     `json:"id"`
	Time       *time.Time `json:"time"`
	ObjectId   *string    `json:"object_id"`
	UserId     *string    `json:"user_id"`
	ElementId  *string    `json:"element_id"`
}

// Converts to a new gRPC `event_v1.EventHeader`.
func (h *EventHeader) ToProto() (header *event_v1.EventHeader) {
	header = &event_v1.EventHeader{}
	header.Type = h.Type
	header.Code = h.Code
	header.SourceType = h.SourceType
	header.SourceId = h.SourceId
	header.Id = h.Id
	if h.Time != nil {
		header.Time = timestamppb.New(*h.Time)
	}
	header.ObjectId = h.ObjectId
	header.UserId = h.UserId
	header.ElementId = h.ElementId

	return header
}

// A simple interface that allows the `SubscriptionManager` to perform
// matching and notification based on the common header, but allows library
// users to define their own event type.
type EventIf interface {
	GetHeader() *EventHeader
}

// A default event implementation that allows library users to store any
// kind of data.
type Event struct {
	Header EventHeader `json:"header"`
	Object interface{} `json:"object"`
}

func (e Event) GetHeader() *EventHeader {
	return &e.Header
}

var eventTypeStrings = map[int32]string{
	0: "unspecified",
	1: "other",
	2: "created",
	3: "updated",
	4: "deleted",
	5: "revoked",
	6: "enabled",
	7: "disabled",
	8: "approved",
	9: "denied",
	10: "scheduled",
	11: "running",
	12: "completed",
	13: "failed",
	14: "succeeded",
	15: "progress",
	16: "suspended",
	17: "resumed",
	18: "started",
	19: "stopped",
	20: "violation",
	21: "interference",
	22: "action",
	23: "pending",
}

func EventTypeString(t int32) string {
	if s, ok := eventTypeStrings[t]; !ok {
		return "unknown"
	} else {
		return s
	}
}

// An implementation of the `zms.event.v1.EventFilter` API definition.
type EventFilter struct {
	Types      []int32  `json:"types" binding:"omitempty"`
	Codes      []int32  `json:"codes" binding:"omitempty"`
	ObjectIds  []string `json:"object_ids" binding:"omitempty,uuid"`
	UserIds    []string `json:"user_ids" binding:"omitempty,uuid"`
	ElementIds []string `json:"element_ids" binding:"omitempty,uuid"`
}

// Converts a gRPC `zms.event.v1.EventFilter` object to our internal
// `EventFilter` representation.
func EventFilterFromProto(pef *event_v1.EventFilter) (ef EventFilter) {
	if pef.Types != nil && len(pef.Types) > 0 {
		ef.Types = make([]int32, 0, len(pef.Types))
		for _, t := range pef.Types {
			ef.Types = append(ef.Types, t)
		}
	}
	if pef.Codes != nil && len(pef.Codes) > 0 {
		ef.Codes = make([]int32, 0, len(pef.Codes))
		for _, c := range pef.Codes {
			ef.Codes = append(ef.Codes, c)
		}
	}
	if pef.ObjectIds != nil && len(pef.ObjectIds) > 0 {
		ef.ObjectIds = make([]string, 0, len(pef.ObjectIds))
		for _, s := range pef.ObjectIds {
			ef.ObjectIds = append(ef.ObjectIds, s)
		}
	}
	if pef.UserIds != nil && len(pef.UserIds) > 0 {
		ef.UserIds = make([]string, 0, len(pef.UserIds))
		for _, s := range pef.UserIds {
			ef.UserIds = append(ef.UserIds, s)
		}
	}
	if pef.ElementIds != nil && len(pef.ElementIds) > 0 {
		ef.ElementIds = make([]string, 0, len(pef.ElementIds))
		for _, s := range pef.ElementIds {
			ef.ElementIds = append(ef.ElementIds, s)
		}
	}

	return ef
}

// Returns `true` if `header` matches this `EventFilter`.
func (f *EventFilter) Match(header *EventHeader) bool {
	if f.Types != nil {
		found := false
		for _, t := range f.Types {
			if header.Type == t {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	if f.Codes != nil {
		found := false
		for _, t := range f.Codes {
			if header.Code == t {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	if header.ObjectId == nil {
		if f.ObjectIds != nil || len(f.ObjectIds) != 0 {
			return false
		}
	} else {
		if f.ObjectIds != nil {
			found := false
			for _, i := range f.ObjectIds {
				if *header.ObjectId == i {
					found = true
					break
				}
			}
			if !found {
				return false
			}
		}
	}
	if header.UserId == nil {
		if f.UserIds != nil || len(f.UserIds) != 0 {
			return false
		}
	} else {
		if f.UserIds != nil {
			found := false
			for _, i := range f.UserIds {
				if *header.UserId == i {
					found = true
					break
				}
			}
			if !found {
				return false
			}
		}
	}
	if header.ElementId == nil {
		if f.ElementIds != nil || len(f.ElementIds) != 0 {
			return false
		}
	} else {
		if f.ElementIds != nil {
			found := false
			for _, i := range f.ElementIds {
				if *header.ElementId == i {
					found = true
					break
				}
			}
			if !found {
				return false
			}
		}
	}

	return true
}
