// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package client

import (
	"context"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
)

// A Subscription is an automatically-maintaining subscription to a target
// gRPC service.  It is essentially a pair of goroutines that maintain a
// subscription at the target service, and read events and invokes the
// associated Handler function until the connection closes, or until the
// `Stop` function is called.  If the connection closes, it will continue to
// reconnect and resubscribe.
type Subscription[T any] struct {
	running        bool
	subscribed     bool
	ctx            context.Context
	ctxCancelFunc  context.CancelFunc
	messageChannel chan *T
	subscribeFunc  SubscribeFunc
	messageHandler MessageHandlerFunc[T]
	statusHandler  StatusHandlerFunc
}

type SubscribeFunc func(context.Context) (cs grpc.ClientStream, abort bool, err error)
type MessageHandlerFunc[T any] func(msg *T) (abort bool)
type StatusHandlerFunc func(connStatus bool, err error) (abort bool)

// Creates a new `Subscription[T]`, where `T` is a message type produced by
// a `grpc.ClientStream`.  `subFunc` is a function that creates the
// subscription, e.g. invokes a method on the target service that results in
// the `grpc.ClientStream` to be read.  `messageHandler` is a function that
// is called each time a message is received on the ClientStream.
// `statusHandler` is called each time the ClientStream is connected
// (`(true, nil)`), and each time there is an error (`(false, err)`).  In
// the latter case, the handler can return `true` to abort the
// Subscription.Run function's goroutines.
func NewSubscription[T any](subFunc SubscribeFunc, messageHandler MessageHandlerFunc[T], statusHandler StatusHandlerFunc) *Subscription[T] {
	return &Subscription[T]{
		messageChannel: make(chan *T),
		subscribeFunc:  subFunc,
		messageHandler: messageHandler,
		statusHandler:  statusHandler,
	}
}

func (s *Subscription[T]) Reset() {
	s.Stop()
	s.ctx = nil
	s.ctxCancelFunc = nil
	s.messageChannel = make(chan *T)
	s.running = false
	s.subscribed = false
}

func (s *Subscription[T]) Stop() {
	if s.running {
		s.ctxCancelFunc()
	}
}

func (s *Subscription[T]) IsRunning() bool {
	return s.running
}

func (s *Subscription[T]) IsSubscribed() bool {
	return s.subscribed
}

// Subscribe, read events, handle, abort, repeat
func (s *Subscription[T]) Run(ctx context.Context) {
	// Set up a cancel context.
	s.ctx, s.ctxCancelFunc = context.WithCancel(ctx)
	s.running = true

	// Read the stream until error and send messages to the channel.
	go func() {
		defer s.ctxCancelFunc()

		for {
			s.subscribed = false
			var cs grpc.ClientStream
			if _cs, abort, err := s.subscribeFunc(s.ctx); err != nil {
				log.Error().Err(err).Msg(fmt.Sprintf("Subscription.Run:sub: subscribeFunc failed: %+v", err))
				if abort {
					return
				} else {
					time.Sleep(4 * time.Second)
					continue
				}
			} else {
				log.Debug().Msg(fmt.Sprintf("Subscription.Run:sub: subscribeFunc succeeded (%+v)", _cs))
				cs = _cs
				s.subscribed = true

				if s.statusHandler != nil {
					s.statusHandler(true, nil)
				}
			}

			for {
				m := new(T)
				if err := cs.RecvMsg(m); err != nil {
					if s.statusHandler != nil {
						if abort := s.statusHandler(true, err); abort {
							log.Debug().Msg(fmt.Sprintf("Subscription.Run:sub: status handler abort"))
							return
						} else {
							log.Debug().Msg(fmt.Sprintf("Subscription.Run:sub: handled recv error: %+v", err))
							break
						}
					} else {
						log.Error().Err(err).Msg(fmt.Sprintf("Subscription.Run:sub: unhandled recv error: %+v", m))
						break
					}
				} else {
					log.Debug().Msg(fmt.Sprintf("Subscription.Run:sub: msg: %+v", m))
					s.messageChannel <- m
				}
			}
		}
	}()

	// Watch both the stop and message channels.  If stop, close the stream
	go func() {
		for {
			select {
			case <-s.ctx.Done():
				log.Debug().Msg(fmt.Sprintf("Subscription.Run:chan: context finished"))
				s.running = false
				s.subscribed = false
				return
			case e := <-s.messageChannel:
				if abort := s.messageHandler(e); abort {
					log.Debug().Msg(fmt.Sprintf("Subscription.Run:chan: messageHandler abort"))
					s.ctxCancelFunc()
					s.running = false
					s.subscribed = false
					return
				} else {
					log.Debug().Msg(fmt.Sprintf("Subscription.Run:chan: messageHandler handled"))
					continue
				}
			}
		}
	}()
}

func DefaultStatusHandler(connStatus bool, err error) (abort bool) {
	return false
}
