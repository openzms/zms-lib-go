// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package client

import (
	"fmt"
	"sort"
	"sync"
	"time"

	"github.com/google/uuid"

	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
)

type CachedRole struct {
	Name  string
	Value int
}

type CachedRoleBinding struct {
	Role      *CachedRole
	ElementId *uuid.UUID
}

type CachedToken struct {
	Id           uuid.UUID
	UserId       uuid.UUID
	Token        string
	IssuedAt     time.Time
	ExpiresAt    *time.Time
	RoleBindings []CachedRoleBinding
}

func (ct *CachedToken) GetElementIds() (uuids []*uuid.UUID) {
	uuids = nil
	for _, rb := range ct.RoleBindings {
		if rb.ElementId == nil {
			continue
		}
		if uuids == nil {
			uuids = make([]*uuid.UUID, 0, len(ct.RoleBindings))
		}
		uuids = append(uuids, rb.ElementId)
	}

	return uuids
}

type TokenCache struct {
	mutex      sync.RWMutex
	Roles      map[uuid.UUID]*CachedRole
	Tokens     map[string]*CachedToken
	TokensById map[string]*CachedToken
}

func NewTokenCache() (tc TokenCache) {
	tc = TokenCache{
		Roles:      make(map[uuid.UUID]*CachedRole),
		Tokens:     make(map[string]*CachedToken),
		TokensById: make(map[string]*CachedToken),
	}
	return tc
}

func (tc *TokenCache) Reset() {
	tc.mutex.Lock()
	tc.Roles = make(map[uuid.UUID]*CachedRole)
	tc.Tokens = make(map[string]*CachedToken)
	tc.TokensById = make(map[string]*CachedToken)
	tc.mutex.Unlock()
}

func (tc *TokenCache) ProtoRoleToCached(in *identity.Role) error {
	var id uuid.UUID
	if idParsed, err := uuid.Parse(in.Id); err != nil {
		return fmt.Errorf("invalid role id: %v", err)
	} else {
		id = idParsed
	}
	cr := &CachedRole{Name: in.Name, Value: int(in.Value)}
	tc.mutex.Lock()
	tc.Roles[id] = cr
	tc.mutex.Unlock()
	return nil
}

func (tc *TokenCache) ProtoTokenToCached(in *identity.Token) (out *CachedToken, err error) {
	out = &CachedToken{}
	if idParsed, err := uuid.Parse(in.Id); err != nil {
		return nil, fmt.Errorf("invalid token id: %v", err)
	} else {
		out.Id = idParsed
	}
	if idParsed, err := uuid.Parse(in.UserId); err != nil {
		return nil, fmt.Errorf("invalid token user id: %v", err)
	} else {
		out.UserId = idParsed
	}
	out.Token = in.Token
	out.IssuedAt = in.IssuedAt.AsTime()
	if in.ExpiresAt != nil {
		t := in.ExpiresAt.AsTime()
		out.ExpiresAt = &t
	}
	for _, rb := range in.RoleBindings {
		var crb CachedRoleBinding
		if rb.ElementId != nil {
			if idParsed, err := uuid.Parse(*rb.ElementId); err != nil {
				return nil, fmt.Errorf("invalid role binding element id: %v", err)
			} else {
				crb.ElementId = &idParsed
			}
		}
		var roleId uuid.UUID
		if idParsed, err := uuid.Parse(rb.RoleId); err != nil {
			return nil, fmt.Errorf("invalid role binding role id: %v", err)
		} else {
			roleId = idParsed
		}
		if cr, exists := tc.Roles[roleId]; !exists {
			return nil, fmt.Errorf("invalid role id %s", roleId.String())
		} else {
			crb.Role = cr
		}

		out.RoleBindings = append(out.RoleBindings, crb)
	}

	//
	// NB: sort the role bindings in descending order, so that policy
	// middleware will match the most encompassing scope first.
	//
	if out.RoleBindings != nil && len(out.RoleBindings) > 0 {
		sort.SliceStable(out.RoleBindings, func(i, j int) bool {
			if out.RoleBindings[i].Role == nil {
				return true
			} else if out.RoleBindings[j].Role == nil {
				return false
			} else if out.RoleBindings[i].Role.Value == out.RoleBindings[j].Role.Value {
				if out.RoleBindings[i].ElementId == nil {
					return false
				} else if out.RoleBindings[j].ElementId == nil {
					return true
				} else {
					return out.RoleBindings[i].ElementId.String() >= out.RoleBindings[j].ElementId.String()
				}
			} else {
				return out.RoleBindings[i].Role.Value >= out.RoleBindings[j].Role.Value
			}
		})
	}

	return out, nil
}

func (tc *TokenCache) Lookup(token string) (t *CachedToken, err error) {
	tc.mutex.RLock()
	ct, exists := tc.Tokens[token]
	tc.mutex.RUnlock()
	if !exists {
		return nil, nil
	}
	if ct.ExpiresAt != nil && ct.ExpiresAt.Compare(time.Now().UTC()) < 1 {
		tc.mutex.Lock()
		if _, exists := tc.Tokens[token]; exists {
			delete(tc.TokensById, tc.Tokens[token].Id.String())
			delete(tc.Tokens, token)
		}
		tc.mutex.Unlock()
		return nil, nil
	}
	return ct, nil
}

func (tc *TokenCache) CheckAndCacheToken(in *identity.Token) (out *CachedToken, err error) {
	out, err = tc.ProtoTokenToCached(in)
	if err != nil {
		return nil, err
	}
	if out.ExpiresAt != nil && out.ExpiresAt.Compare(time.Now().UTC()) < 1 {
		return nil, fmt.Errorf("token expired")
	}
	tc.mutex.Lock()
	tc.Tokens[out.Token] = out
	tc.TokensById[in.Id] = out
	tc.mutex.Unlock()

	return out, nil
}

func (tc *TokenCache) Delete(token string) {
	tc.mutex.RLock()
	_, exists := tc.Tokens[token]
	tc.mutex.RUnlock()
	if !exists {
		return
	}
	tc.mutex.Lock()
	if ct, exists := tc.Tokens[token]; exists {
		delete(tc.TokensById, ct.Id.String())
		delete(tc.Tokens, token)
	}
	tc.mutex.Unlock()
	return
}

func (tc *TokenCache) DeleteById(tokenId string) {
	tc.mutex.RLock()
	_, exists := tc.TokensById[tokenId]
	tc.mutex.RUnlock()
	if !exists {
		return
	}
	tc.mutex.Lock()
	if ct, exists := tc.TokensById[tokenId]; exists {
		delete(tc.Tokens, ct.Token)
		delete(tc.TokensById, tokenId)
	}
	tc.mutex.Unlock()
	return
}
