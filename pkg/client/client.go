// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package client

import (
	"context"
	"fmt"
	"sync"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"

	alarm "gitlab.flux.utah.edu/openzms/zms-api/go/zms/alarm/v1"
	dst "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	event "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"
	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"
)

type ZmsClientConfig struct {
	IdentityRpcEndpoint   string
	WatchIdentityServices bool
	WatchIdentityTokens   bool
}

type ZmsClient struct {
	mutex                sync.RWMutex
	config               ZmsClientConfig
	identityClient       *identity.IdentityClient
	identitySecret       *string
	identitySubscription *Subscription[identity.SubscribeResponse]
	tokenCache           TokenCache
	bootstrapElementId   *uuid.UUID
	bootstrapUserId      *uuid.UUID
	bootstrapZone        *zmc.Zone
	zmcService           *identity.Service
	zmcClient            *zmc.ZmcClient
	dstService           *identity.Service
	dstClient            *dst.DstClient
	alarmService         *identity.Service
	alarmClient          *alarm.AlarmClient
	serviceMapById       map[string]*identity.Service
	serviceMapByKind     map[string][]*identity.Service
	clients              map[string]interface{}

	cacheLock  sync.RWMutex
	radios     map[uuid.UUID]*zmc.Radio
	radioPorts map[uuid.UUID]*zmc.RadioPort
	monitors   map[uuid.UUID]*zmc.Monitor
}

func New(config ZmsClientConfig) (rclient ZmsClient) {
	rclient = ZmsClient{
		config: config,
	}
	rclient.serviceMapById = make(map[string]*identity.Service)
	rclient.serviceMapByKind = make(map[string][]*identity.Service)
	rclient.clients = make(map[string]interface{})
	rclient.tokenCache = NewTokenCache()

	rclient.radios = make(map[uuid.UUID]*zmc.Radio)
	rclient.radioPorts = make(map[uuid.UUID]*zmc.RadioPort)
	rclient.monitors = make(map[uuid.UUID]*zmc.Monitor)

	return rclient
}

func (rclient *ZmsClient) GetIdentityClient(ctx context.Context) (identity.IdentityClient, error) {
	if rclient.identityClient != nil {
		return *rclient.identityClient, nil
	}

	if conn, err := grpc.Dial(
		rclient.config.IdentityRpcEndpoint,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		//grpc.WithKeepaliveParams(keepalive.ClientParameters{PermitWithoutStream: true})
	); err != nil {
		return nil, fmt.Errorf("failed to connect to identity service (%s): %v", rclient.config.IdentityRpcEndpoint, err)
	} else {
		c := identity.NewIdentityClient(conn)
		rclient.identityClient = &c
	}

	return *rclient.identityClient, nil
}

func (rclient *ZmsClient) GetZmcClient(ctx context.Context) (c zmc.ZmcClient, err error) {
	if rclient.zmcClient != nil {
		return *rclient.zmcClient, nil
	}

	if rclient.zmcService == nil {
		if err = rclient.UpdateServiceList(ctx); err != nil {
			return nil, fmt.Errorf("failed to update service list to find zmc: %+v", err)
		}
	}
	if rclient.zmcService == nil {
		return nil, fmt.Errorf("service zmc not registered at identity service")
	}

	if conn, err := grpc.Dial(
		rclient.zmcService.Endpoint,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		//grpc.WithKeepaliveParams(keepalive.ClientParameters{PermitWithoutStream: true})
	); err != nil {
		return nil, fmt.Errorf("failed to connect to zmc service (%s): %v", rclient.zmcService.Endpoint, err)
	} else {
		//defer conn.Close()
		c := zmc.NewZmcClient(conn)
		rclient.zmcClient = &c
	}

	return *rclient.zmcClient, nil
}

func (rclient *ZmsClient) GetDstClient(ctx context.Context) (c dst.DstClient, err error) {
	if rclient.dstClient != nil {
		return *rclient.dstClient, nil
	}

	if rclient.dstService == nil {
		if err = rclient.UpdateServiceList(ctx); err != nil {
			return nil, fmt.Errorf("failed to update service list to find dst: %+v", err)
		}
	}
	if rclient.dstService == nil {
		return nil, fmt.Errorf("service dst not registered at identity service")
	}

	if conn, err := grpc.Dial(
		rclient.dstService.Endpoint,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		//grpc.WithKeepaliveParams(keepalive.ClientParameters{PermitWithoutStream: true})
	); err != nil {
		return nil, fmt.Errorf("failed to connect to dst service (%s): %v", rclient.dstService.Endpoint, err)
	} else {
		//defer conn.Close()
		c := dst.NewDstClient(conn)
		rclient.dstClient = &c
	}

	return *rclient.dstClient, nil
}

func (rclient *ZmsClient) GetAlarmClient(ctx context.Context) (c alarm.AlarmClient, err error) {
	if rclient.alarmClient != nil {
		return *rclient.alarmClient, nil
	}

	if rclient.alarmService == nil {
		if err = rclient.UpdateServiceList(ctx); err != nil {
			return nil, fmt.Errorf("failed to update service list to find alarm: %+v", err)
		}
	}
	if rclient.alarmService == nil {
		return nil, fmt.Errorf("service alarm not registered at identity service")
	}

	if conn, err := grpc.Dial(
		rclient.alarmService.Endpoint,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		//grpc.WithKeepaliveParams(keepalive.ClientParameters{PermitWithoutStream: true})
	); err != nil {
		return nil, fmt.Errorf("failed to connect to alarm service (%s): %v", rclient.alarmService.Endpoint, err)
	} else {
		//defer conn.Close()
		c := alarm.NewAlarmClient(conn)
		rclient.alarmClient = &c
	}

	return *rclient.alarmClient, nil
}

func (rclient *ZmsClient) RegisterService(ctx context.Context, service *identity.Service) (err error) {
	var iclient identity.IdentityClient
	if iclient, err = rclient.GetIdentityClient(ctx); err != nil {
		return err
	}

	var resp *identity.RegisterServiceResponse
	if resp, err = iclient.RegisterService(ctx, &identity.RegisterServiceRequest{Service: service}); err != nil {
		return fmt.Errorf("failed to register at identity service (%s): %v", rclient.config.IdentityRpcEndpoint, err)
	}

	rclient.identitySecret = &resp.Secret
	if resp.BootstrapElementId != "" {
		if idParsed, err := uuid.Parse(resp.BootstrapElementId); err == nil {
			rclient.bootstrapElementId = &idParsed
		}
	}
	if resp.BootstrapUserId != "" {
		if idParsed, err := uuid.Parse(resp.BootstrapUserId); err == nil {
			rclient.bootstrapUserId = &idParsed
		}
	}
	log.Debug().Msg(fmt.Sprintf("registered at %s: %s", rclient.config.IdentityRpcEndpoint, resp.Secret))

	// Grab initial identity service role list.
	if r, err := iclient.GetRoleList(ctx, &identity.RoleListRequest{}); err != nil {
		return fmt.Errorf("failed to get roles from identity service: %v", err)
	} else {
		for _, role := range r.Roles {
			rclient.tokenCache.ProtoRoleToCached(role)
		}
	}

	// Watch the identity service's list of services via events.
	if rclient.config.WatchIdentityServices || rclient.config.WatchIdentityTokens {
		rclient.IdentityWatcher(ctx, false)
	}

	return nil
}

func (rclient *ZmsClient) GetServices(kind string) (ret []*identity.Service) {
	rclient.mutex.RLock()
	defer rclient.mutex.RUnlock()

	ret = make([]*identity.Service, 0)
	if v, ok := rclient.serviceMapByKind[kind]; ok {
		for _, s := range v {
			ret = append(ret, s)
		}
	}
	return ret
}

func (rclient *ZmsClient) NewIdentitySubscription(mh MessageHandlerFunc[identity.SubscribeResponse], filters []*event.EventFilter, elaborate *bool, include *bool) (s *Subscription[identity.SubscribeResponse]) {
	sf := func(ctx context.Context) (cs grpc.ClientStream, abort bool, err error) {
		var iclient identity.IdentityClient
		if iclient, err = rclient.GetIdentityClient(ctx); err != nil {
			return nil, false, err
		}

		reqId := uuid.New().String()
		hdr := identity.RequestHeader{
			ReqId:     &reqId,
			Elaborate: elaborate,
		}
		req := identity.SubscribeRequest{
			Header:  &hdr,
			Filters: filters,
			Include: include,
		}
		log.Debug().Msg(fmt.Sprintf("identity.Subscribe(%s)", reqId))
		if cs, err := iclient.Subscribe(ctx, &req); err != nil {
			log.Warn().Msg(fmt.Sprintf("identity.Subscribe(%s) error: %+v", reqId, err))
			return nil, false, err
		} else {
			log.Debug().Msg(fmt.Sprintf("identity.Subscribe(%s) success", reqId))
			return cs, false, nil
		}
	}
	return NewSubscription[identity.SubscribeResponse](sf, mh, nil)
}

func (rclient *ZmsClient) IdentityWatcherStatusHandler(connected bool, err error) (abort bool) {
	if !connected {
		log.Warn().Msg(fmt.Sprintf("identity watcher: disconnected (%+v)", err))
	} else {
		log.Info().Msg(fmt.Sprintf("identity watcher: connected: %+v", connected))
	}

	// Reset the token cache.
	rclient.tokenCache.Reset()

	// NB: eventually reset other cached items, possibly connections to
	// other services as well, to handle wholesale service plane
	// reconfiguration and restart.

	return false
}

func (rclient *ZmsClient) IdentityWatcherMessageHandler(resp *identity.SubscribeResponse) (abort bool) {
	if resp.Events == nil {
		return
	}

	for _, e := range resp.Events {
		switch e.Header.Code {
		case int32(identity.EventCode_EC_TOKEN):
			if e.Header.Type == int32(event.EventType_ET_DELETED) || e.Header.Type == int32(event.EventType_ET_REVOKED) {
				rclient.tokenCache.DeleteById(e.Header.Id)
				log.Info().Msg(fmt.Sprintf("IdentityWatcher: token revoked: %s", e.Header.Id))
			}
		case int32(identity.EventCode_EC_SERVICE):
			var service *identity.Service
			if service = e.GetService(); service == nil {
				log.Warn().Msg(fmt.Sprintf("unexpected nil Service object"))
				continue
			}
			if e.Header.Type == int32(event.EventType_ET_CREATED) || e.Header.Type == int32(event.EventType_ET_UPDATED) || e.Header.Type == int32(event.EventType_ET_ENABLED) {
				rclient.mutex.Lock()
				if _, exists := rclient.serviceMapById[service.Id]; !exists {
					rclient.serviceMapById[service.Id] = service
					if _, ok := rclient.serviceMapByKind[service.Kind]; !ok {
						rclient.serviceMapByKind[service.Kind] = make([]*identity.Service, 0, 1)
					}
					rclient.serviceMapByKind[service.Kind] = append(rclient.serviceMapByKind[service.Kind], service)
				}

				// Handle singleton services specially.
				if service.Kind == "zmc" {
					if rclient.zmcService == nil {
						log.Debug().Msg(fmt.Sprintf("new zmc service: %+v", service))
						rclient.zmcService = service
					} else if rclient.zmcService.Id != service.Id || rclient.zmcService.Endpoint != service.Endpoint {
						log.Debug().Msg(fmt.Sprintf("zmc service updated: %+v -> %+v", rclient.zmcService, service))
						rclient.zmcService = service
						rclient.zmcClient = nil
					}
				} else if service.Kind == "dst" {
					if rclient.dstService == nil {
						log.Debug().Msg(fmt.Sprintf("new dst service: %+v", service))
						rclient.dstService = service
					} else if rclient.dstService.Id != service.Id || rclient.dstService.Endpoint != service.Endpoint {
						log.Debug().Msg(fmt.Sprintf("dst service updated: %+v -> %+v", rclient.dstService, service))
						rclient.dstService = service
						rclient.dstClient = nil
					}
				} else if service.Kind == "alarm" {
					if rclient.alarmService == nil {
						log.Debug().Msg(fmt.Sprintf("new alarm service: %+v", service))
						rclient.alarmService = service
					} else if rclient.alarmService.Id != service.Id || rclient.alarmService.Endpoint != service.Endpoint {
						log.Debug().Msg(fmt.Sprintf("alarm service updated: %+v -> %+v", rclient.alarmService, service))
						rclient.alarmService = service
						rclient.alarmClient = nil
					}
				}
				rclient.mutex.Unlock()
			}
		default:
			log.Debug().Msg(fmt.Sprintf("IdentityWatcher: message %+v", e))
		}
	}

	return false
}

func (rclient *ZmsClient) IdentityWatcher(ctx context.Context, force bool) (err error) {
	if rclient.identitySubscription != nil && rclient.identitySubscription.IsRunning() {
		return nil
	}

	filters := make([]*event.EventFilter, 0, 2)
	if rclient.config.WatchIdentityServices || force {
		f := &event.EventFilter{
			Types: []int32{int32(event.EventType_ET_CREATED), int32(event.EventType_ET_UPDATED)},
			Codes: []int32{int32(identity.EventCode_EC_SERVICE)},
		}
		filters = append(filters, f)
	}
	if rclient.config.WatchIdentityTokens || force {
		f := &event.EventFilter{
			Types: []int32{int32(event.EventType_ET_REVOKED)},
			Codes: []int32{int32(identity.EventCode_EC_SERVICE)},
		}
		filters = append(filters, f)
	}

	t := true
	rclient.identitySubscription = rclient.NewIdentitySubscription(
		rclient.IdentityWatcherMessageHandler, filters, &t, &t)
	rclient.identitySubscription.Run(context.TODO())

	return nil
}

func (rclient *ZmsClient) GetBootstrapInfo() (elementId *uuid.UUID, userId *uuid.UUID) {
	return rclient.bootstrapElementId, rclient.bootstrapUserId
}

func (rclient *ZmsClient) UpdateServiceList(ctx context.Context) (err error) {
	var iclient identity.IdentityClient
	if iclient, err = rclient.GetIdentityClient(ctx); err != nil {
		return err
	}

	var resp *identity.ServiceListResponse
	if resp, err = iclient.GetServiceList(ctx, &identity.ServiceListRequest{}); err != nil {
		return fmt.Errorf("failed to get services from identity service (%s): %v", rclient.config.IdentityRpcEndpoint, err)
	}

	rclient.mutex.Lock()

	localServiceMap := make(map[string]*identity.Service)
	for _, service := range resp.Services {
		localServiceMap[service.Id] = service
		if _, ok := rclient.serviceMapById[service.Id]; ok {
			continue
		}

		rclient.serviceMapById[service.Id] = service
		if _, ok := rclient.serviceMapByKind[service.Kind]; !ok {
			rclient.serviceMapByKind[service.Kind] = make([]*identity.Service, 0, 1)
		}
		rclient.serviceMapByKind[service.Kind] = append(rclient.serviceMapByKind[service.Kind], service)

		// Handle singleton services specially.
		if service.Kind == "zmc" {
			if rclient.zmcService == nil {
				log.Debug().Msg(fmt.Sprintf("new zmc service: %+v", service))
				rclient.zmcService = service
			} else if rclient.zmcService.Id != service.Id || rclient.zmcService.Endpoint != service.Endpoint {
				log.Debug().Msg(fmt.Sprintf("zmc service updated: %+v -> %+v", rclient.zmcService, service))
				rclient.zmcService = service
				rclient.zmcClient = nil
			}
		} else if service.Kind == "dst" {
			if rclient.dstService == nil {
				log.Debug().Msg(fmt.Sprintf("new dst service: %+v", service))
				rclient.dstService = service
			} else if rclient.dstService.Id != service.Id || rclient.dstService.Endpoint != service.Endpoint {
				log.Debug().Msg(fmt.Sprintf("dst service updated: %+v -> %+v", rclient.dstService, service))
				rclient.dstService = service
				rclient.dstClient = nil
			}
		} else if service.Kind == "alarm" {
			if rclient.alarmService == nil {
				log.Debug().Msg(fmt.Sprintf("new alarm service: %+v", service))
				rclient.alarmService = service
			} else if rclient.alarmService.Id != service.Id || rclient.alarmService.Endpoint != service.Endpoint {
				log.Debug().Msg(fmt.Sprintf("alarm service updated: %+v -> %+v", rclient.alarmService, service))
				rclient.alarmService = service
				rclient.alarmClient = nil
			}
		}
	}

	// Delete stale services
	for _, existing := range rclient.serviceMapById {
		if _, ok := localServiceMap[existing.Id]; !ok {
			delete(rclient.serviceMapById, existing.Id)
			var i int
			for i = 0; i < len(rclient.serviceMapByKind[existing.Kind]); i += 1 {
				if rclient.serviceMapByKind[existing.Kind][i].Id == existing.Id {
					break
				}
			}
			if i < len(rclient.serviceMapByKind[existing.Kind]) {
				rclient.serviceMapByKind[existing.Kind] = append(rclient.serviceMapByKind[existing.Kind][:i], rclient.serviceMapByKind[existing.Kind][i+1:]...)
			}
			if _, ok := rclient.clients[existing.Id]; ok {
				delete(rclient.clients, existing.Id)
			}

			if rclient.zmcService != nil && rclient.zmcService.Id == existing.Id {
				rclient.zmcService = nil
				rclient.zmcClient = nil
			}
			if rclient.dstService != nil && rclient.dstService.Id == existing.Id {
				rclient.dstService = nil
				rclient.dstClient = nil
			}
			if rclient.alarmService != nil && rclient.alarmService.Id == existing.Id {
				rclient.alarmService = nil
				rclient.alarmClient = nil
			}
		}
	}

	rclient.mutex.Unlock()
	return nil
}

func (rclient *ZmsClient) LookupToken(token string) (t *CachedToken, err error) {
	return rclient.tokenCache.Lookup(token)
}

func (rclient *ZmsClient) CheckAndCacheToken(in *identity.Token) (out *CachedToken, err error) {
	return rclient.tokenCache.CheckAndCacheToken(in)
}

func (rclient *ZmsClient) NewZmcSubscription(mh MessageHandlerFunc[zmc.SubscribeResponse], sh StatusHandlerFunc, filters []*event.EventFilter, elaborate *bool, include *bool) (s *Subscription[zmc.SubscribeResponse]) {
	sf := func(ctx context.Context) (cs grpc.ClientStream, abort bool, err error) {
		var dclient zmc.ZmcClient
		if dclient, err = rclient.GetZmcClient(ctx); err != nil {
			return nil, false, err
		}

		reqId := uuid.New().String()
		hdr := zmc.RequestHeader{
			ReqId:     &reqId,
			Elaborate: elaborate,
		}
		req := zmc.SubscribeRequest{
			Header:  &hdr,
			Filters: filters,
			Include: include,
		}
		log.Debug().Msg(fmt.Sprintf("zmc.Subscribe(%s)", reqId))
		if cs, err := dclient.Subscribe(ctx, &req); err != nil {
			log.Warn().Msg(fmt.Sprintf("zmc.Subscribe(%s) error: %+v", reqId, err))
			return nil, false, err
		} else {
			log.Debug().Msg(fmt.Sprintf("zmc.Subscribe(%s) success", reqId))
			return cs, false, nil
		}
	}
	return NewSubscription[zmc.SubscribeResponse](sf, mh, sh)
}

func (rclient *ZmsClient) GetBootstrapZone(ctx context.Context, elaborate bool) (o *zmc.Zone, err error) {
	rclient.cacheLock.RLock()
	o = rclient.bootstrapZone
	rclient.cacheLock.RUnlock()
	if o != nil {
		return o, nil
	}

	var zmcClient zmc.ZmcClient
	if zmcClient, err = rclient.GetZmcClient(ctx); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}
	reqId := uuid.New().String()
	hdr := zmc.RequestHeader{
		ReqId:     &reqId,
		Elaborate: &elaborate,
	}
	req := zmc.GetZoneRequest{
		Header: &hdr,
		Id:     uuid.Nil.String(),
	}
	if resp, err := zmcClient.GetZone(ctx, &req); err != nil {
		return nil, err
	} else {
		o = resp.Zone
	}

	rclient.cacheLock.Lock()
	rclient.bootstrapZone = o
	rclient.cacheLock.Unlock()

	return o, nil
}

func (rclient *ZmsClient) GetMonitor(ctx context.Context, id *uuid.UUID, elaborate bool) (o *zmc.Monitor, err error) {
	rclient.cacheLock.RLock()
	o, exists := rclient.monitors[*id]
	rclient.cacheLock.RUnlock()
	if exists {
		return o, nil
	}

	var zmcClient zmc.ZmcClient
	if zmcClient, err = rclient.GetZmcClient(ctx); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}
	reqId := uuid.New().String()
	hdr := zmc.RequestHeader{
		ReqId:     &reqId,
		Elaborate: &elaborate,
	}
	req := zmc.GetMonitorRequest{
		Header: &hdr,
		Id:     id.String(),
	}
	if resp, err := zmcClient.GetMonitor(ctx, &req); err != nil {
		return nil, err
	} else {
		o = resp.Monitor
	}

	rclient.cacheLock.Lock()
	rclient.monitors[*id] = o
	rclient.cacheLock.Unlock()

	return o, nil
}

func (rclient *ZmsClient) GetRadio(ctx context.Context, id *uuid.UUID, elaborate bool) (o *zmc.Radio, err error) {
	rclient.cacheLock.RLock()
	o, exists := rclient.radios[*id]
	rclient.cacheLock.RUnlock()
	if exists {
		return o, nil
	}

	var zmcClient zmc.ZmcClient
	if zmcClient, err = rclient.GetZmcClient(ctx); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}
	reqId := uuid.New().String()
	hdr := zmc.RequestHeader{
		ReqId:     &reqId,
		Elaborate: &elaborate,
	}
	req := zmc.GetRadioRequest{
		Header: &hdr,
		Id:     id.String(),
	}
	if resp, err := zmcClient.GetRadio(ctx, &req); err != nil {
		return nil, err
	} else {
		o = resp.Radio
	}

	rclient.cacheLock.Lock()
	rclient.radios[*id] = o
	rclient.cacheLock.Unlock()

	return o, nil
}

func (rclient *ZmsClient) GetRadioPort(ctx context.Context, id *uuid.UUID, elaborate bool) (o *zmc.RadioPort, err error) {
	rclient.cacheLock.RLock()
	o, exists := rclient.radioPorts[*id]
	rclient.cacheLock.RUnlock()
	if exists {
		return o, nil
	}

	var zmcClient zmc.ZmcClient
	if zmcClient, err = rclient.GetZmcClient(ctx); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}
	reqId := uuid.New().String()
	hdr := zmc.RequestHeader{
		ReqId:     &reqId,
		Elaborate: &elaborate,
	}
	req := zmc.GetRadioPortRequest{
		Header: &hdr,
		Id:     id.String(),
	}
	if resp, err := zmcClient.GetRadioPort(ctx, &req); err != nil {
		return nil, err
	} else {
		o = resp.RadioPort
	}

	rclient.cacheLock.Lock()
	rclient.radioPorts[*id] = o
	rclient.cacheLock.Unlock()

	return o, nil
}

func (rclient *ZmsClient) NewDstSubscription(mh MessageHandlerFunc[dst.SubscribeResponse], sh StatusHandlerFunc, filters []*event.EventFilter, elaborate *bool, include *bool) (s *Subscription[dst.SubscribeResponse]) {
	sf := func(ctx context.Context) (cs grpc.ClientStream, abort bool, err error) {
		var dclient dst.DstClient
		if dclient, err = rclient.GetDstClient(ctx); err != nil {
			return nil, false, err
		}

		reqId := uuid.New().String()
		hdr := dst.RequestHeader{
			ReqId:     &reqId,
			Elaborate: elaborate,
		}
		req := dst.SubscribeRequest{
			Header:  &hdr,
			Filters: filters,
			Include: include,
		}
		log.Debug().Msg(fmt.Sprintf("dst.Subscribe(%s)", reqId))
		if cs, err := dclient.Subscribe(ctx, &req); err != nil {
			log.Warn().Msg(fmt.Sprintf("dst.Subscribe(%s) error: %+v", reqId, err))
			return nil, false, err
		} else {
			log.Debug().Msg(fmt.Sprintf("dst.Subscribe(%s) success", reqId))
			return cs, false, nil
		}
	}
	return NewSubscription[dst.SubscribeResponse](sf, mh, sh)
}

func (rclient *ZmsClient) NewAlarmSubscription(mh MessageHandlerFunc[alarm.SubscribeResponse], sh StatusHandlerFunc, filters []*event.EventFilter, elaborate *bool, include *bool) (s *Subscription[alarm.SubscribeResponse]) {
	sf := func(ctx context.Context) (cs grpc.ClientStream, abort bool, err error) {
		var dclient alarm.AlarmClient
		if dclient, err = rclient.GetAlarmClient(ctx); err != nil {
			return nil, false, err
		}

		reqId := uuid.New().String()
		hdr := alarm.RequestHeader{
			ReqId:     &reqId,
			Elaborate: elaborate,
		}
		req := alarm.SubscribeRequest{
			Header:  &hdr,
			Filters: filters,
			Include: include,
		}
		log.Debug().Msg(fmt.Sprintf("alarm.Subscribe(%s)", reqId))
		if cs, err := dclient.Subscribe(ctx, &req); err != nil {
			log.Warn().Msg(fmt.Sprintf("alarm.Subscribe(%s) error: %+v", reqId, err))
			return nil, false, err
		} else {
			log.Debug().Msg(fmt.Sprintf("alarm.Subscribe(%s) success", reqId))
			return cs, false, nil
		}
	}
	return NewSubscription[alarm.SubscribeResponse](sf, mh, sh)
}
