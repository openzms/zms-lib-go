<!--
SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# zms-lib-go (OpenZMS golang library support)

The [OpenZMS software](https://gitlab.flux.utah.edu/openzms) is a prototype
automatic spectrum-sharing management system for radio dynamic zones.
OpenZMS provides mechanisms to share electromagnetic (radio-frequency)
spectrum between experimental or test systems and existing spectrum users,
and between multiple experimental systems.  We are building and deploying
OpenZMS within the context of the POWDER wireless testbed in Salt Lake City,
Utah, part of the NSF-sponsored Platforms for Advanced Wireless Research
program, to create [POWDER-RDZ](https://rdz.powderwireless.net).

This repository contains `golang` library code for OpenZMS services.

## Quick start

Build:

```
go build -v ./...
```

Test:

```
go test -v ./...
```
